package postcomment;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.servlet.http.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

@WebServlet("/postcomment")
public class PostComment extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final String STRINGLIST = "stringlist";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> stringList = (List<String>) session.getAttribute(STRINGLIST);
		if (stringList == null) {
			stringList = new ArrayList<>();
		}

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try (PrintWriter out = response.getWriter()) {
			out.println("<html><head><title>Post it</title></head><body>");
			out.println("<div>");
			for (String s : stringList)
				out.println(s);
			out.println("</div>");
			out.println("<form method='POST'><h3>Enter a new String</h3>"
					+ "</br><input type='text' name='comment' ><input type = submit value='Enter your Post'></form>");
			out.println("</body></html>");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> stringList = (List<String>) session.getAttribute(STRINGLIST);
		if (stringList == null) {
			System.out.println(stringList);
			stringList = new ArrayList<>();
		}
		;
		stringList.add(request.getParameter("comment"));
		session.setAttribute(STRINGLIST, stringList);
		doGet(request, response);
	}

}
