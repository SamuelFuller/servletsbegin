package postcomment;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Visitors")
public class VisitorServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		request.getSession();
		System.out.println("VisitorServlet");
		int total = (Integer) getServletContext().getAttribute(VisitorSessionListener.TOTAL);
	
		int active = (Integer) getServletContext().getAttribute(VisitorSessionListener.ACTIVE);
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try (PrintWriter out = response.getWriter()) {
			out.println("<!DOCTYPE html>");
			out.println("<html><head><title>");
			out.println("Visitors");
			out.println("</title></head>");
			out.println("<body>");
			out.println("Total visitors: " + total + "<br/>");
			out.println("Current visitors: " + active + "<br/>");
			out.println("</body></html>");

		}
	}

}