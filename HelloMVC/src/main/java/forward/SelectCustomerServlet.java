package forward;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SelectCustomer")
public class SelectCustomerServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String page = "SelectConsumer.html";
		String consumption = req.getParameter("consumption");
		if (consumption != null) {
			int cons;
			try {
				cons = Integer.parseInt(consumption);
				if (cons < 2000) {
					page = "SmallConsumer.html";
				} else {
					page = "BigConsumer.html";
				}
			} catch (NumberFormatException ex) {
				page = "Invalid.html";
			}
		}

		RequestDispatcher disp = req.getRequestDispatcher(page);
		disp.forward(req, resp);
	}
}
