package hellobean;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/HelloMVC")
public class HelloMVCServlet extends HttpServlet {

	private HelloService service = new HelloService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("HelloForm.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HelloBean hb = new HelloBean();
		hb.setName(req.getParameter("name"));
		service.handleHello(hb);
		req.setAttribute("helloBean", hb);
		req.getRequestDispatcher("HelloResult.jsp").forward(req, resp);
	}
}
