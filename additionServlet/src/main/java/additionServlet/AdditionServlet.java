package additionServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Addition")
public class AdditionServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String answer;
		try{
			int number1 = Integer.parseInt(request.getParameter("number1"));
			int number2 = Integer.parseInt(request.getParameter("number2"));
			answer = "The sum is" + (number1 + number2);
		} catch  (NumberFormatException ex){
			answer = "Invalid number";
		}
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		try(PrintWriter out = response.getWriter()){
			out.println("<!DOCTYPE html>");
			out.print("<html><head><title>Addition Servlet");
			out.print("</title></head><body>");
			out.print(answer);
			out.print("</body></html>");
		}
	}
}
