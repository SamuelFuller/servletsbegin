package testTwee;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/testtwee")
public class TestTwee extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String name = req.getParameter("name");
		final String NAMING = "naming";
		HttpSession session = req.getSession();

		System.out.println(session);

		if (session.getAttribute(NAMING) == null) {
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			try (PrintWriter out = resp.getWriter()) {
				out.println("<html><head><title>Test Twee een sessie</title></head><body>");
				out.println("<div>");
				out.println("hello ");
				out.print(name);
				out.println("</div>");
				out.println("</body></html>");
			}
		} else {
			try (PrintWriter out = resp.getWriter()) {
				out.println("<html><head><title>Test Twee een sessie</title></head><body>");
				out.println("<div>");
				out.println("logged on as:");
				out.print(name);
				out.println("</div>");
				out.println("</body></html>");
			}
		}

		session.setAttribute(NAMING, name);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
