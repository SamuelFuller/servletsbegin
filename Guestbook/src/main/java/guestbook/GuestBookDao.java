package guestbook;

import java.sql.*;
import java.util.*;
import guestbook.*;

public class GuestBookDao {
	private String url;
	private String user;
	private String password;
	private static final String GET_QUERY = "select Date, Name, Message from GuestBook order by Date limit ? offset ?";
	private static final String UPDATE_QUERY = "insert into GuestBook (Date, Name, Message) values (?,?,?)";
	private static final String SIZE_QUERY = "select count(*) from GuestBook";

	public void setDriver(String driver) throws ClassNotFoundException {
		Class.forName(driver);
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	// select Date, Name, Message from GuestBook order by Date limit ? offset ?

	public List<GuestBookBean> getGuestBookItems(int limit, int offset) throws SQLException {
		List<GuestBookBean> messages = new ArrayList<GuestBookBean>();
		try (Connection con = getConnection()) {
			PreparedStatement stmt = con.prepareStatement(GET_QUERY);
			stmt.setInt(1, limit);
			stmt.setInt(2, offset);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				GuestBookBean bean = new GuestBookBean(rs.getTimestamp(1).toLocalDateTime(), rs.getString(2),
						rs.getString(3));
				messages.add(bean);
				System.out.println(bean);
			}
			return messages;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return messages;

	}

	public int getTotaalAantalItems() {
		int size = 0;
		try (Connection con = getConnection();
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery(SIZE_QUERY);) {

			if (rs.next()) {
				size = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return size;
	}

	public void addGuestBookItem(GuestBookBean item) throws SQLException {
		try (Connection con = getConnection(); PreparedStatement stmt = con.prepareStatement(UPDATE_QUERY)) {
			stmt.setTimestamp(1, Timestamp.valueOf(item.getDate()));
			stmt.setString(2, item.getName());
			stmt.setString(3, item.getMessage());
			stmt.executeUpdate();
		}
	}

	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}

}
