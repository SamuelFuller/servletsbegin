package guestbook;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = "/GuestBook", initParams = { @WebInitParam(name = "driver", value = "com.mysql.jdbc.Driver"),
		@WebInitParam(name = "url", value = "jdbc:mysql://noelvaes.eu/GuestBookDB"),
		@WebInitParam(name = "user", value = "student"), @WebInitParam(name = "password", value = "student123") })

public class GuestBookServlet extends HttpServlet {
	private GuestBookDao dao;

	private final int RECORDS_PER_PAGE = 10;
	

	public void init() throws ServletException {
		try {
			String driver = getInitParameter("driver");
			String url = getInitParameter("url");
			String user = getInitParameter("user");
			String password = getInitParameter("password");

			if (driver == null || url == null || user == null || password == null) {
				throw new ServletException("Init parameter missing");
			}

			dao = new GuestBookDao();
			dao.setDriver(driver);
			dao.setUrl(url);
			dao.setUser(user);
			dao.setPassword(password);
		} catch (ClassNotFoundException ex) {
			throw new ServletException("Unable to initialize DAO ", ex);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		int totaalAantalPaginas = 0;
		int currentPage = 0;
		int offset = 0;
		int limit = RECORDS_PER_PAGE;
		
		
		try (PrintWriter out = response.getWriter()) {
			
			
			int totaalAantalItems = dao.getTotaalAantalItems();
			System.out.println("totaal:" + totaalAantalItems);
			totaalAantalPaginas = (totaalAantalItems / RECORDS_PER_PAGE);
			if ((totaalAantalItems % RECORDS_PER_PAGE) > 0) {
				totaalAantalPaginas++;
			}
			offset=(totaalAantalPaginas * RECORDS_PER_PAGE)-RECORDS_PER_PAGE;
			System.out.println(offset);
			currentPage = offset / RECORDS_PER_PAGE;
			System.out.println(currentPage);
			
			try {
				limit = Integer.valueOf(request.getParameter("limit"));
				offset = Integer.valueOf(request.getParameter("offset"));
			} catch (NumberFormatException nfe) {
			}
			System.out.println("limit:" + limit);
			System.out.println("offset:" + offset);
			List<GuestBookBean> items = dao.getGuestBookItems(limit, offset);
			
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");

			out.println("<html><head>");
			out.println("<title>Guestbook</title>");
			out.println("</head><body>");

			System.out.println(items);
			
			items.forEach(i -> out.format("<p>[%td/%<tm/%<tY %<tT] %s:%s</p>", i.getDate(), i.getName(),
					i.getMessage().replaceAll("&", "&amp;").replaceAll("<", "&lt;")));

			if (currentPage > 0)
				out.print("<a href='GuestBook?limit=" + RECORDS_PER_PAGE + "&offset=" + (offset - RECORDS_PER_PAGE)
					+ "'>previous</a>");

			if (currentPage < totaalAantalPaginas)
				out.print("<a href='GuestBook?limit=" + RECORDS_PER_PAGE + "&offset=" + (offset + RECORDS_PER_PAGE)
						+ "'>next</a>");

			String action = request.getParameter("action");

			out.print("<form method='POST' >");
			out.print("Name: ");
			out.print("<input type='text' name='name' /><br />");
			out.print("<textarea cols='50' rows='10' name='message'>");
			out.print("</textarea>");
			out.print("<br/><input type='submit' value='Submit' />");
			out.print("</form></body></html>");
		} catch (SQLException e) {
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setCharacterEncoding("UTF-8");
			String name = request.getParameter("name");
			String message = request.getParameter("message");
			GuestBookBean item = new GuestBookBean();
			item.setDate(LocalDateTime.now());
			item.setMessage(message);
			item.setName(name);
			dao.addGuestBookItem(item);
			log("Message added: " + name + " : " + message);
			doGet(request, response);
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}
	}
}
