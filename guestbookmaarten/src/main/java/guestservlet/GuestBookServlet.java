package guestservlet;

import static sessions.SessionKeeper.INDEX;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.GuestBookBean;
import dao.GuestBookDao;

@WebServlet(value = "/guestservlet")
public class GuestBookServlet extends HttpServlet {

	private final String SHOWLASTTEN = "lastTen";

	static GuestBookDao gbd;

	public GuestBookServlet() {
		gbd = GuestBookDao.getInstance();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gbd.setUrl("jdbc:mysql://noelvaes.eu/GuestBookDB");
		gbd.setUser("student");
		gbd.setPassword("student123");
		List<GuestBookBean> guestList = gbd.getGuestBookItems();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int index;
		HttpSession sc = request.getSession();
		Integer indexInteger = (Integer) sc.getAttribute(INDEX);
		index = indexInteger;

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		try (PrintWriter out = response.getWriter()) {
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title> GuestBook </title>");
			out.println(
					"<style> body { background-color: pink; font color=red} h1 { color: maroon; margin-left: 40px; } </style>");
			out.println("</head>");
			out.println("<body>");

			List<GuestBookBean> guestList = gbd.getGuestBookItems();

			for (int i = index; i < index + 10; i++) {
				out.println(guestList.get(i) + "<br/>");
			}
			out.println("<form method='POST'>");
			out.println("Geef uw naam enzo in");
			out.println("<br/> <input type='text' name='naam'>");
			out.println("<br/> <input type='text' name='message'>");
			out.println("<input type='submit' name='submit' value='submit'> </form>");
			out.println("<form method='POST'>");
			out.println("vorige tien resultaten");
			out.println("<input type='submit' name='lastTen' value='lastTen'> </form>");
			out.println("<form method='POST'>");
			out.println("volgende tien resultaten");
			out.println("<input type='submit' name='nextTen' value='nextTen'>");
			out.println("</form>");
			out.println("</body></html>");
		}
	}

//	protected void showLastTen(HttpServletRequest request, HttpServletResponse response) {
//
//	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int index;
		HttpSession sc = request.getSession();
		Integer indexInteger = (Integer) sc.getAttribute(INDEX);
		index = indexInteger;
		List<GuestBookBean> guestList = gbd.getGuestBookItems();
		if (request.getParameter("submit") != null) {
			String naam = request.getParameter("naam");
			String message = request.getParameter("message");
			if (message != "" && naam != "") {
				Timestamp stamp = new Timestamp(System.currentTimeMillis());
				Date date = new Date(stamp.getTime());
				GuestBookBean gb = new GuestBookBean(date, naam, message);
				gbd.addGuestBookItem(gb);
				guestList.add(gb);
				index++;
				updateSession(index, request);
			}
			doGet(request, response);
		} else if (request.getParameter("lastTen") != null) {
			index -= 10;
			if (index < 0) {
				index = 0;
			}
			updateSession(index, request);
			doGet(request, response);
		} else if (request.getParameter("nextTen") != null) {
			index += 10;
			if (index >= guestList.size()) {
				index = guestList.size() - 10;
			}
			updateSession(index, request);
			doGet(request, response);
		}
	}

	private void updateSession(int index, HttpServletRequest request) {
		HttpSession sc = request.getSession();
		sc.setAttribute(INDEX, index);
	}

}