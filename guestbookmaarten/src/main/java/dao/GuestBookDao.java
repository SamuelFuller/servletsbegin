package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.GuestBookBean;

public class GuestBookDao {
	private static List<GuestBookBean> guestList;
	private static GuestBookDao instance;

	public static GuestBookDao getInstance() {
		if (instance == null) {
			return new GuestBookDao();
		} else
			return instance;
	}

	private GuestBookDao() {

	}

	private String driver;
	private String url;
	private String user;
	private String password;

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean addGuestBookItem(GuestBookBean item) {
		try (Connection con = getConnection();
				PreparedStatement stmt = con.prepareStatement("insert into GuestBook(Date, Name, Message) values('"
						+ item.getTimeStamp() + "', '" + item.getName() + "', '" + item.getMessage() + "')")) {
			if (stmt.executeUpdate() != 0) {
				guestList.add(item);
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public List<GuestBookBean> getGuestBookItems() {
		try (Connection con = getConnection();
				PreparedStatement stmt = con.prepareStatement("select * from GuestBook")) {
			ResultSet rslt = stmt.executeQuery();
			guestList = new ArrayList<>();
			while (rslt.next()) {
				guestList.add(
						new GuestBookBean(rslt.getDate("Date"), rslt.getString("Name"), rslt.getString("Message")));
			}
			return guestList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private Connection getConnection() {
		try {

			Connection con = DriverManager.getConnection(url, user, password);

			return con;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

}