package beans;


import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

public class GuestBookBean {
	private Date timeStamp;
	private String name;
	private String message;
	
	
	public Timestamp getTimeStamp() {
		
		Timestamp ts = new Timestamp(timeStamp.getTime());
		return ts;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = new Date(timeStamp);
		System.out.println(timeStamp);

	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;


	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public GuestBookBean(Date date, String name, String message) {
		setTimeStamp(date);
		
		this.name = name;
		this.message = message;
	}

	@Override
	public String toString() {
		return  name
				+ " - said: " + message + " - at: " + timeStamp;
	}
	
	
	
}