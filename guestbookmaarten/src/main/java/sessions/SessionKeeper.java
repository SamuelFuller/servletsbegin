package sessions;

import java.util.List;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import beans.GuestBookBean;
import dao.GuestBookDao;

@WebListener
public class SessionKeeper implements HttpSessionListener {

	public static final String INDEX = "SessionKeeper.index";
	static GuestBookDao gbd;

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("NEW SESSION CREATED");
		HttpSession sc = se.getSession();
		Integer index = (Integer) sc.getAttribute(INDEX);
		gbd = GuestBookDao.getInstance();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gbd.setUrl("jdbc:mysql://noelvaes.eu/GuestBookDB");
		gbd.setUser("student");
		gbd.setPassword("student123");
		List<GuestBookBean> guestList = gbd.getGuestBookItems();
		index = guestList.size() - 10;
		if (index < 0) {
			index = 0;
		}

		sc.setAttribute(INDEX, index);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {

	}

}