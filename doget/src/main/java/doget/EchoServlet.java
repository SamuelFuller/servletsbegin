package doget;


import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.http.*;



public class EchoServlet extends HttpServlet{
	protected void doGEt(HttpServletRequest request, HttpServletResponse response)throws IOException{
		String text = request.getParameter("text");
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		try(PrintWriter out = response.getWriter()){
			out.println("<!DOCTYPE html>");
			out.println("<html><head><title>Echo Servlet");
			out.println("</title></head></body>");
			out.println(text);
			out.println("</body></html>");
		}
	}

}
