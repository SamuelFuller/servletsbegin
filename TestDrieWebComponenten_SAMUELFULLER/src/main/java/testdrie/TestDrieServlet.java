package testdrie;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/TestDrieServlet")
public class TestDrieServlet extends HttpServlet {

	final String SESSIONUSERLOGIN = "sessionuserlogin";
	final String HELLOGUEST = "helloguest";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String loginName = (String) req.getSession().getAttribute(SESSIONUSERLOGIN);
		String page = null;
		HttpSession session = req.getSession();
		String hello = "Hello Guest";
		session.setAttribute(HELLOGUEST,hello);

		if (loginName == null) {
			page = "/WEB-INF/pages/login.jsp";
		} else {
			page = "/WEB-INF/pages/welcome.jsp";
		}

		req.getRequestDispatcher(page).forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();
		String loginName = (String) session.getAttribute(SESSIONUSERLOGIN);

		String name = req.getParameter("name");

		session.setAttribute(SESSIONUSERLOGIN, name);

		if (name == null) {

			session.setAttribute(HELLOGUEST, "Hello Guest, gelieve een geldige naam in te voeren");
			req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);

		} else

			resp.sendRedirect(req.getRequestURI());

	}

}
