package greetings;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "GreetingsServlet", value = "/HelloWorld", initParams = @WebInitParam(name = "greeting", value = "Hello World") )

public class Greetings extends HttpServlet {
	private String greeting;

	@Override
	public void init() throws ServletException {
		log("servlet is initialised");
		greeting = getInitParameter("greeting");
		if(greeting == null)
		throw new ServletException("Parameter text not found");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		try (PrintWriter out = response.getWriter()) {
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Hello Servlet</title>");
			out.println("</head>");
			out.println("<body>");
			out.println(greeting);
			out.println("</body>");
			out.println("</html>");
		}
	}
	

	public void destroy() {
		log("servlet is destroyed");
	}
}
